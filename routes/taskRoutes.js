const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

// Create single task
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

// Update a task
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})


//Delete a task
router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})

module.exports = router


//S36-Activity
// Getting Specific Task

router.get('/:id/', (request, response) => {
	TaskController.specificTask(request.params.id).then((result) => {
		response.send(result)
	})
})


// Changing the status of a task to complete

router.patch('/:id/complete', (request, response) => {
	TaskController.completeTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

