const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoute = require('./routes/taskRoutes')

//Initialize dotenv
dotenv.config()

// Server setup
const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//MongoDB connection
mongoose.connect(`mongodb+srv://aysonrizamayp:${process.env.MONGODB_PASSWORD}@cluster0.y2tm4qs.mongodb.net/s36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error("Connection Error"))
db.on('open', () => console.error("Connected to MongoDB!"))

//Routes
app.use('/tasks', taskRoute)

//Server listening
app.listen(port, () => console.log(`Server running on localhost:${port}`))
